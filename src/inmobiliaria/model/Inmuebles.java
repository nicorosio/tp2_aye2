package inmobiliaria.model;

public class Inmuebles {

	private int id;
	
	private String calle;

	private int numero;

	private int piso;

	private char puerta;

	private int tipoPropiedad;

	private int estadoConservacion;

	private int estadoInmueble;

	private int vista;

	private int orientacion;

	private int metrosTotales;

	private int metrosCubiertos;

	private int metrosDescubiertos;

	private int cantDormitorios;

	private int cantBanos;

	private int cantPlantas;

	private int cantAscensores;

	private int luminosidad;

	private int vigilancia;

	private boolean pileta;

	private boolean balcon;

	private boolean patio;

	private int tipoOperacion;

	private Agentes agente;

	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}
	
	public String getCalle() {
		return calle;
	}

	public void setCalle(String calle) {
		this.calle = calle;
	}

	public int getNumero() {
		return numero;
	}

	public void setNumero(int numero) {
		this.numero = numero;
	}

	public int getPiso() {
		return piso;
	}

	public void setPiso(int piso) {
		this.piso = piso;
	}

	public char getPuerta() {
		return puerta;
	}

	public void setPuerta(char puerta) {
		this.puerta = puerta;
	}

	public int getTipoPropiedad() {
		return tipoPropiedad;
	}

	public void setTipoPropiedad(int tipoPropiedad) {
		this.tipoPropiedad = tipoPropiedad;
	}

	public int getEstadoConservacion() {
		return estadoConservacion;
	}

	public void setEstadoConservacion(int estadoConservacion) {
		this.estadoConservacion = estadoConservacion;
	}

	public int getEstadoInmueble() {
		return estadoInmueble;
	}

	public void setEstadoInmueble(int estadoInmueble) {
		this.estadoInmueble = estadoInmueble;
	}

	public int getVista() {
		return vista;
	}

	public void setVista(int vista) {
		this.vista = vista;
	}

	public int getOrientacion() {
		return orientacion;
	}

	public void setOrientacion(int orientacion) {
		this.orientacion = orientacion;
	}

	public int getMetrosTotales() {
		return metrosTotales;
	}

	public void setMetrosTotales(int metrosTotales) {
		this.metrosTotales = metrosTotales;
	}

	public int getMetrosCubiertos() {
		return metrosCubiertos;
	}

	public void setMetrosCubiertos(int metrosCubiertos) {
		this.metrosCubiertos = metrosCubiertos;
	}

	public int getMetrosDescubiertos() {
		return metrosDescubiertos;
	}

	public void setMetrosDescubiertos(int metrosDescubiertos) {
		this.metrosDescubiertos = metrosDescubiertos;
	}

	public int getCantDormitorios() {
		return cantDormitorios;
	}

	public void setCantDormitorios(int cantDormitorios) {
		this.cantDormitorios = cantDormitorios;
	}

	public int getCantBanos() {
		return cantBanos;
	}

	public void setCantBanos(int cantBanos) {
		this.cantBanos = cantBanos;
	}

	public int getCantPlantas() {
		return cantPlantas;
	}

	public void setCantPlantas(int cantPlantas) {
		this.cantPlantas = cantPlantas;
	}

	public int getCantAscensores() {
		return cantAscensores;
	}

	public void setCantAscensores(int cantAscensores) {
		this.cantAscensores = cantAscensores;
	}

	public int getLuminosidad() {
		return luminosidad;
	}

	public void setLuminosidad(int luminosidad) {
		this.luminosidad = luminosidad;
	}

	public int getVigilancia() {
		return vigilancia;
	}

	public void setVigilancia(int vigilancia) {
		this.vigilancia = vigilancia;
	}

	public boolean isPileta() {
		return pileta;
	}

	public void setPileta(boolean pileta) {
		this.pileta = pileta;
	}

	public boolean isBalcon() {
		return balcon;
	}

	public void setBalcon(boolean balcon) {
		this.balcon = balcon;
	}

	public boolean isPatio() {
		return patio;
	}

	public void setPatio(boolean patio) {
		this.patio = patio;
	}

	public int getTipoOperacion() {
		return tipoOperacion;
	}

	public void setTipoOperacion(int tipoOperacion) {
		this.tipoOperacion = tipoOperacion;
	}

	public Agentes getAgente() {
		return agente;
	}

	public void setAgente(Agentes agente) {
		this.agente = agente;
	}	
	
}
