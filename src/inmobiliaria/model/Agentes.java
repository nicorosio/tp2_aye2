package inmobiliaria.model;

public class Agentes extends Persona {

	private int numeroAgente;

	private int cuil;

	private double sueldoBasico;

	public int getNumeroAgente() {
		return numeroAgente;
	}

	public void setNumeroAgente(int numeroAgente) {
		this.numeroAgente = numeroAgente;
	}

	public int getCuil() {
		return cuil;
	}

	public void setCuil(int cuil) {
		this.cuil = cuil;
	}

	public double getSueldoBasico() {
		return sueldoBasico;
	}

	public void setSueldoBasico(double sueldoBasico) {
		this.sueldoBasico = sueldoBasico;
	}
	
}
