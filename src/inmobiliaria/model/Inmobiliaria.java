package inmobiliaria.model;

import java.util.ArrayList;
import java.util.List;

import inmobiliaria.utils.Utils;

public class Inmobiliaria {

    private List<Agentes> agentes = new ArrayList<>();
    
    private List<Inmuebles> inmuebles = new ArrayList<>();
    

    public Inmobiliaria() {
        //TODO constructor con datos de la inmobiliaria
    }

    public void addAgente(Agentes agente){
    	agentes.add(agente);
    }

    public void addInmueble(Inmuebles inmueble){
        inmuebles.add(inmueble);
    }

    public List<Inmuebles> buscarInmueble(String propiedad, int value) {
    	List<Inmuebles> resultado = new ArrayList<>();
    	Utils utils = new Utils();
    	switch (propiedad) {
    		case "id":
    			resultado = Utils.findListByProperty(this.inmuebles, inmueble -> value == inmueble.getId());
    	}
    	return resultado;
    }
    

}
