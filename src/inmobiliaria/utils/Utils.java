package inmobiliaria.utils;

import java.util.ArrayList;
import java.util.Collection;
import java.util.List;
import java.util.function.Predicate;

public class Utils {
	
	public static <T> T findElementByProperty(Collection<T> col, Predicate<T> filter) {
	    return col.stream().filter(filter).findFirst().orElse(null);
	}
	
	public static <T> List<T> findListByProperty(Collection<T> col, Predicate<T> filter) {
		List<T> result = new ArrayList<T>();
	    for (T element: col) {
	        if (filter.test(element)) {
	            result.add(element);
	        }
	    }
	    return result;
	}
}
