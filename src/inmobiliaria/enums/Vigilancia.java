package inmobiliaria.enums;

public enum Vigilancia {
	SIN_VIGILANCIA(1), DIURNO(2), NOCTURNO(3), HORAS_24(4);

	private int value;

	Vigilancia(int value) {
		this.value = value;
	}

	public int getValue() {
		return this.value;
	}
}
