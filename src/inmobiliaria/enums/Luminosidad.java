package inmobiliaria.enums;

public enum Luminosidad {
    ALTA(1),
    MEDIA(2),
    BAJA(3);

    private Integer value;

    Luminosidad(Integer value) {
        this.value = value;
    }

    public int getValue() {
		return this.value;
	}
}
