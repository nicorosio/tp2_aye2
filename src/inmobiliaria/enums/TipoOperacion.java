package inmobiliaria.enums;

public enum TipoOperacion {
    
    ALQUILER(1), VENTA(2);

	private int value;

	TipoOperacion(int value) {
		this.value = value;
	}

	public int getValue() {
		return this.value;
	}
}
