package inmobiliaria.enums;

public enum TipoPropiedad {

	ESTUDIO(1), LOFT(2), DEPARTAMENTO(3), PISO(4), DUPLEX(5), TRIPLEX(6), CHALET(7), CASA(8), LOCA(9), COCHERA(10),
	OFICINA(11), EDIFICIO(12);

	private int value;

	TipoPropiedad(int value) {
		this.value = value;
	}

	public int getValue() {
		return this.value;
	}
}
