package inmobiliaria.enums;

public enum EstadoConservacion {

	ESTRENAR(1), RECICLAR(2), IMPECABLE(3), MUY_BUENO(4), BUENO(5);

    private Integer value;

    EstadoConservacion(Integer value) {
        this.value = value;
    }

    public int getValue() {
		return this.value;
	}
	
}
