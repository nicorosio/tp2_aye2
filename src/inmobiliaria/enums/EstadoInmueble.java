package inmobiliaria.enums;

public enum EstadoInmueble {

    HABITADO_PROPIETARIO(1),
    DESHABITADO(2),
    CONSTRUCTION(3),
    HABITADO_INQUILINO(4);

    private Integer value;

    EstadoInmueble(Integer value) {
        this.value = value;
    }

    public int getValue() {
		return this.value;
	}

}