package inmobiliaria.enums;

public enum Vista {

	EXTERIOR(1), INTERIOR(2), AMBOS(3);

    private Integer value;

    Vista(Integer value) {
        this.value = value;
    }

    public int getValue() {
		return this.value;
	}
	
}
