package inmobiliaria.enums;

public enum Orientacion {

	NORTE(1), SUR(2), ESTE(3), OESTE(4), NORESTE(5), NOROESTE(6), SURESTE(7), SUROESTE(8);
	
	private Integer value;

	Orientacion(Integer value) {
		this.value = value;
	}

	public int getValue() {
		return this.value;
	}

}
